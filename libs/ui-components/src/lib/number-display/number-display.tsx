import styles from './number-display.module.scss';

/* eslint-disable-next-line */
export interface NumberDisplayProps {
  digits?: string
}

export function NumberDisplay(props: NumberDisplayProps) {
  return (
      <div className={styles['screen']}>
        <h1 className={styles['digits']}>{(props.digits === '') ? 'Enter a number then press enter...' : props.digits}</h1>
      </div>
  );
}

export default NumberDisplay;
