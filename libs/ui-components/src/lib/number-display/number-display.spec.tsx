import { render } from '@testing-library/react';

import NumberDisplay from './number-display';

describe('NumberDisplay', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NumberDisplay />);
    expect(baseElement).toBeTruthy();
  });
});
