import { ComponentStory, ComponentMeta } from '@storybook/react';
import { NumberDisplay } from './number-display';

export default {
  component: NumberDisplay,
  title: 'NumberDisplay',
} as ComponentMeta<typeof NumberDisplay>;

const Template: ComponentStory<typeof NumberDisplay> = (args) => (
  <NumberDisplay {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
