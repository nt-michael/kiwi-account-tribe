import styles from './t9-button.module.scss';

/* eslint-disable-next-line */
export interface T9ButtonProps {
  label: string;
  digit: string;
  background?: string;
  color?: string;
  radius?: string;
  height?: string;
  width?: string;
  displayVal(e:string):void
}

export function simpleFx(e:Event) {
  console.log(e)
}

export function T9Button(props: T9ButtonProps) {
  return (
    <button className={styles['btn']} onClick={()=>props.displayVal(props.digit)}
        style={{
          color: props.color,
          backgroundColor: props.background,
          borderRadius: props.radius,
          height: props.height,
          width: props.width,
        }}>
        {(Boolean(props.digit)) && <span>{props.digit}</span>}
        {(Boolean(props.label)) && <span>{props.label}</span>}
      </button>
  );
}

export default T9Button;
