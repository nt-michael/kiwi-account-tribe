import { render } from '@testing-library/react';

import T9Button from './t9-button';

describe('T9Button', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<T9Button label={''} digit={''} displayVal={function (e: string): void {
      throw new Error('Function not implemented.');
    } } />);
    expect(baseElement).toBeTruthy();
  });
});
