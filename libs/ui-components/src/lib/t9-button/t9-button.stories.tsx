import { ComponentStory, ComponentMeta } from '@storybook/react';
import { T9Button } from './t9-button';

export default {
  component: T9Button,
  title: 'T9Button',
} as ComponentMeta<typeof T9Button>;

const Template: ComponentStory<typeof T9Button> = (args) => (
  <T9Button {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};
