import { render } from '@testing-library/react';

import Phonewords from './phonewords';

describe('Phonewords', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Phonewords />);
    expect(baseElement).toBeTruthy();
  });
});
