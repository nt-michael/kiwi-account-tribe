import T9Button from 'libs/ui-components/src/lib/t9-button/t9-button';
import { useState } from 'react';
import { NumberDisplay } from '../../../../../libs/ui-components/src/lib/number-display/number-display';
import styles from './phonewords.module.scss';
// import {  } from "@apollo/client/react/hooks/";
import { gql, useQuery } from "@apollo/client";


/* eslint-disable-next-line */
export interface PhonewordsProps { }

export interface T9Btn {
  value: string;
  text: string;
}

const GQLQUERYY = gql`
  query Digit($digits: String!)
  {
    generate(q: $digits) {
      combinations
    }
  }
  `;

export default function Phonewords(props: PhonewordsProps) {

  const phonepad: T9Btn[] = [{ value: '1', text: '' }, { value: '2', text: 'ABC' }, { value: '3', text: 'DEF' }, { value: '4', text: 'GHI' }, { value: '5', text: 'JKL' }, { value: '6', text: 'MNO' }, { value: '7', text: 'PQRS' }, { value: '8', text: 'TUV' }, { value: '9', text: 'WXYZ' }, { value: 'Clear', text: '' }, { value: '0', text: '' }, { value: 'Go', text: '' }];

  const [displayDigits, setDisplay] = useState('');

  const { loading, data, error, refetch } = useQuery(GQLQUERYY, { variables: { digits: '' } });

  const displayVal = (val: string) => {
    const validDigits = ['2', '3', '4', '5', '6', '7', '8', '9'];
    if (val === 'Clear') setDisplay('');
    if (validDigits.includes(val)) setDisplay(displayDigits + val);
    if (val === 'Go') {
      const digits = displayDigits;
      refetch({ digits });
    }
  }

  if (loading) return <p>Loading...</p>;
  if (error) return <p>{error.message}</p>

  return (
    <div className={styles['t9-wrapper']}>
      <div className={styles['phone-container']}>
        <NumberDisplay digits={displayDigits} />
        <div className={styles['t9-buttons-wrapper']}>
          {phonepad.map((t9) => <T9Button key={t9.value} digit={t9.value} label={t9.text} displayVal={(e) => displayVal(e)} />)}
        </div>
        <div className={styles['t9-combinations']}>
          <h2>Possible Combinations</h2>
          <span>
            {data.generate.combinations.map((e: string) => e).join(", ")}
          </span>
        </div>
      </div>
    </div>
  );
}