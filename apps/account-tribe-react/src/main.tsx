import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';
import { ApolloProvider,  ApolloClient, InMemoryCache } from "@apollo/client";

import App from './app/app';
import { environment } from './environments/environment';

const client = new ApolloClient({
  uri: `${environment.baseurl}/graphql/`,
  cache: new InMemoryCache()
});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <ApolloProvider client={client}>
    <StrictMode>
      <App />
  </StrictMode>
  </ApolloProvider>
);
