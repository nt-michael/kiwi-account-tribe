export const KeyboardMapper: KeyboardMapper = {
  1: '',
  2: 'abc',
  3: 'def',
};

type KeyboardMapper = {
  [key: number]: string;
};
