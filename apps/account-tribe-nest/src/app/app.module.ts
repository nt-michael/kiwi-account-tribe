import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PhonewordsModule } from './phonewords/phonewords.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
    }),
    PhonewordsModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'account-tribe-react'),
      exclude: ['/api*','/graphql']
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
