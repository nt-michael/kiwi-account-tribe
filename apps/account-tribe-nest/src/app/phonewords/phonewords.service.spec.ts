import { Test, TestingModule } from '@nestjs/testing';
import { PhonewordsService } from './phonewords.service';

describe('PhonewordsService', () => {
  let service: PhonewordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PhonewordsService],
    }).compile();

    service = module.get<PhonewordsService>(PhonewordsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
