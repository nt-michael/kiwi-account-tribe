import { Injectable } from '@nestjs/common';
import { GetPhoneDigits } from './model/phoneworddto';

@Injectable()
export class PhonewordsService {
    /**
   * T9 letter dictionanry arranged from 0 to 9.
   * Noting that 0 and 1 don't have any corresponding word or letter in the T9 convention
   */
  private DICTIONARY:string[] = [
    '',
    '',
    'abc',
    'def',
    'ghi',
    'jkl',
    'mno',
    'pqrs',
    'tuv',
    'wxyz',
  ];

  getData(): { message: string } {
    return { message: 'Welcome to account-tribe-nest!' };
  }

  /**
   * @author Michaël Nde
   * @description Returns a list of all possible way to get a T9 word
   * @param {string} value Contains digits to be converted
   * @returns {string[]}
   */
  generateCombinations(getPhoneDigits: GetPhoneDigits): string[] {
    const result: string[] = [];
    this.getRecursive('', getPhoneDigits.q, 0, result);
    return result;
  }

  /**
   * @author Michaël Nde
   * @description Recursively resolves all the different possible combinations of a T9 word
   * @param prefix
   * @param digits
   * @param idx
   * @param res
   * @returns {string[]}
   */
  getRecursive(
    prefix: string,
    digits: string,
    idx: number,
    res: string[]
  ): string[] {
    if (idx >= digits.length) {
      res.push(prefix);
      return;
    }
    const letters = this.DICTIONARY[digits[idx]];
    for (let i = 0; i < letters.length; i++) {
      this.getRecursive(prefix + letters[i], digits, idx + 1, res);
    }
  }
}
