import { Module } from '@nestjs/common';
import { PhonewordsService } from './phonewords.service';
import { PhonewordsResolver } from './phonewords.resolver';

@Module({
  providers: [PhonewordsService, PhonewordsResolver],
})
export class PhonewordsModule {}
