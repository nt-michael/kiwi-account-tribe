import { Resolver, Query, Args } from '@nestjs/graphql';
import { GetPhoneDigits, PhonewordResDto } from './model/phoneworddto';
import { PhonewordsService } from './phonewords.service';

@Resolver(() => PhonewordResDto)
export class PhonewordsResolver {
  constructor(private readonly phonewordService: PhonewordsService) {}

  @Query(() => PhonewordResDto, { name: 'generate' })
  getCombinations(@Args() getPhoneDigits: GetPhoneDigits): PhonewordResDto {
    return {
      combinations: this.phonewordService.generateCombinations(getPhoneDigits),
    };
  }
}
