import { Test, TestingModule } from '@nestjs/testing';
import { PhonewordsResolver } from './phonewords.resolver';
import { PhonewordsService } from './phonewords.service';

describe('PhonewordsResolver', () => {
  let resolver: PhonewordsResolver;
  // let phonewordService: PhonewordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PhonewordsResolver, PhonewordsService],
    }).compile();

    resolver = module.get<PhonewordsResolver>(PhonewordsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
