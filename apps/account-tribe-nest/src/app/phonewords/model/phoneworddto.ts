import { ArgsType, Field, ObjectType} from "@nestjs/graphql";
import { IsNotEmpty } from "class-validator";

@ArgsType()
export class GetPhoneDigits {
    @Field()
    @IsNotEmpty()
    q: string;
}

@ObjectType()
export class PhonewordResDto {
    @Field(() => [String])
    combinations: string[]
}